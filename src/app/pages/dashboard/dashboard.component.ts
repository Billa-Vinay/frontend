import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AddEditStoryComponent } from 'src/app/add-edit-story/add-edit-story.component';
import { ApiServiceService } from 'src/app/api-service.service';
import { TYPE } from 'src/app/constants';
import { PeriodicElement } from 'src/app/modal/PeriodicElement';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, AfterViewInit{

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['edit', 'delete', 'storyName', 'storyDescription', 'storyRemarks', 'userName', 'updatedDate'];
  dataSource = new MatTableDataSource<any>();
  users: any[]=[];
  isAdmin: boolean = false;
  userId: number ;
  rowData: any = [];

  constructor(private http: HttpClient, private service: ApiServiceService, private dialog: MatDialog) { 
  }
  
  ngOnInit(): void {
    let retrievedObject = localStorage.getItem('userObject');
    this.isAdmin = JSON.parse(retrievedObject)?.role === 'ROLE_ADMIN';
    this.userId = JSON.parse(retrievedObject)?.id;
    this.getStories();    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getStories() {
    if(this.isAdmin){
      this.service.getStories(null).subscribe((res:any)=>{
        if(res?.length === 0){
          this.service.toast(TYPE.INFO, true, 'No stories available. Trying adding a few.');
        }
        this.rowData = res;
        this.dataSource = new MatTableDataSource<any>(this.rowData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.service.getStories(String(this.userId)).subscribe((res:any)=>{
        if(res?.length === 0){
          this.service.toast(TYPE.INFO, true, 'No stories available. Trying adding a few.');
        }
        this.rowData = res;
        this.dataSource = new MatTableDataSource<any>(this.rowData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }
  }

  openDialog(data: any) {
    this.dialog.open(AddEditStoryComponent, { data: data, width: '500px', disableClose: true }).afterClosed().subscribe(res => {
      if (res) {
        this.getStories();
      }
     });
  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  } 
  
  deleteStory(params: any){
    this.service.deleteStory(params.id).subscribe(res => {
      this.service.toast(TYPE.SUCCESS, true, res?.message);
      this.getStories();
    }, error => {
      this.service.toast(TYPE.ERROR, true, error?.error);
    })
  }

}
